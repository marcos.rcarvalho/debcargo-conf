This patch is based on the upstream commit described below, adapted for use
in the Debian package by Peter Michael Green.

commit edd0f60d0baf09604553525c2636df5d6ba05d44
Author: Ian Douglas Scott <idscott@system76.com>
Date:   Fri Sep 22 17:47:30 2023 -0700

    Use `rustix` instead of `nix`

Index: wayland-client/src/conn.rs
===================================================================
--- wayland-client.orig/src/conn.rs
+++ wayland-client/src/conn.rs
@@ -2,7 +2,7 @@ use std::{
     env, fmt,
     io::ErrorKind,
     os::unix::net::UnixStream,
-    os::unix::prelude::{AsRawFd, FromRawFd},
+    os::unix::io::{AsFd, AsRawFd, BorrowedFd, FromRawFd, OwnedFd},
     path::PathBuf,
     sync::{
         atomic::{AtomicBool, Ordering},
@@ -12,12 +12,9 @@ use std::{
 
 use wayland_backend::{
     client::{Backend, InvalidId, ObjectData, ObjectId, ReadEventsGuard, WaylandError},
-    io_lifetimes::OwnedFd,
     protocol::{ObjectInfo, ProtocolError},
 };
 
-use nix::{fcntl, Error};
-
 use crate::{protocol::wl_display::WlDisplay, EventQueue, Proxy};
 
 /// The Wayland connection
@@ -50,21 +47,21 @@ impl Connection {
         let stream = if let Ok(txt) = env::var("WAYLAND_SOCKET") {
             // We should connect to the provided WAYLAND_SOCKET
             let fd = txt.parse::<i32>().map_err(|_| ConnectError::InvalidFd)?;
+            let fd = unsafe { OwnedFd::from_raw_fd(fd) };
             // remove the variable so any child processes don't see it
             env::remove_var("WAYLAND_SOCKET");
             // set the CLOEXEC flag on this FD
-            let flags = fcntl::fcntl(fd, fcntl::FcntlArg::F_GETFD);
+            let flags = rustix::io::fcntl_getfd(&fd);
             let result = flags
-                .map(|f| fcntl::FdFlag::from_bits(f).unwrap() | fcntl::FdFlag::FD_CLOEXEC)
-                .and_then(|f| fcntl::fcntl(fd, fcntl::FcntlArg::F_SETFD(f)));
+                .map(|f| f | rustix::io::FdFlags::CLOEXEC)
+                .and_then(|f| rustix::io::fcntl_setfd(&fd, f));
             match result {
                 Ok(_) => {
                     // setting the O_CLOEXEC worked
-                    unsafe { FromRawFd::from_raw_fd(fd) }
+                    UnixStream::from(fd)
                 }
                 Err(_) => {
                     // something went wrong in F_GETFD or F_SETFD
-                    let _ = ::nix::unistd::close(fd);
                     return Err(ConnectError::InvalidFd);
                 }
             }
@@ -155,7 +152,7 @@ impl Connection {
             crate::protocol::wl_display::Request::Sync {},
             Some(done.clone()),
         )
-        .map_err(|_| WaylandError::Io(Error::EPIPE.into()))?;
+        .map_err(|_| WaylandError::Io(rustix::io::Errno::PIPE.into()))?;
 
         let mut dispatched = 0;
 
@@ -222,15 +219,16 @@ impl Connection {
 }
 
 pub(crate) fn blocking_read(guard: ReadEventsGuard) -> Result<usize, WaylandError> {
-    let mut fds = [nix::poll::PollFd::new(
-        guard.connection_fd().as_raw_fd(),
-        nix::poll::PollFlags::POLLIN | nix::poll::PollFlags::POLLERR,
+    let fd = guard.connection_fd();
+    let mut fds = [rustix::event::PollFd::new(
+        &fd,
+        rustix::event::PollFlags::IN | rustix::event::PollFlags::ERR,
     )];
 
     loop {
-        match nix::poll::poll(&mut fds, -1) {
+        match rustix::event::poll(&mut fds, -1) {
             Ok(_) => break,
-            Err(nix::errno::Errno::EINTR) => continue,
+            Err(rustix::io::Errno::INTR) => continue,
             Err(e) => return Err(WaylandError::Io(e.into())),
         }
     }
Index: wayland-client/src/event_queue.rs
===================================================================
--- wayland-client.orig/src/event_queue.rs
+++ wayland-client/src/event_queue.rs
@@ -5,7 +5,6 @@ use std::marker::PhantomData;
 use std::sync::{atomic::Ordering, Arc, Condvar, Mutex};
 use std::task;
 
-use nix::Error;
 use wayland_backend::{
     client::{Backend, ObjectData, ObjectId, ReadEventsGuard, WaylandError},
     io_lifetimes::OwnedFd,
@@ -421,7 +420,7 @@ impl<State> EventQueue<State> {
                 crate::protocol::wl_display::Request::Sync {},
                 Some(done.clone()),
             )
-            .map_err(|_| WaylandError::Io(Error::EPIPE.into()))?;
+            .map_err(|_| WaylandError::Io(rustix::io::Errno::PIPE.into()))?;
 
         let mut dispatched = 0;
 
Index: wayland-client/Cargo.toml
===================================================================
--- wayland-client.orig/Cargo.toml
+++ wayland-client/Cargo.toml
@@ -49,9 +49,9 @@ optional = true
 version = "0.4"
 optional = true
 
-[dependencies.nix]
-version = "0.26.0"
-default-features = false
+[dependencies.rustix]
+version = "0.38.0"
+features = ["event"]
 
 [dependencies.wayland-backend]
 version = "0.1.0"
Index: wayland-client/src/calloop.rs
===================================================================
--- wayland-client.orig/src/calloop.rs
+++ wayland-client/src/calloop.rs
@@ -11,7 +11,6 @@ use calloop::{
     generic::Generic, EventSource, InsertError, Interest, LoopHandle, Mode, Poll, PostAction,
     Readiness, RegistrationToken, Token, TokenFactory,
 };
-use nix::errno::Errno;
 use wayland_backend::client::{ReadEventsGuard, WaylandError};
 
 /// An adapter to insert an [`EventQueue`] into a calloop [`EventLoop`](calloop::EventLoop).
@@ -188,7 +187,7 @@ impl<D> WaylandSource<D> {
                 Err(DispatchError::Backend(WaylandError::Protocol(err))) => {
                     log_error!("Protocol error received on display: {}", err);
 
-                    break Err(Errno::EPROTO.into());
+                    break Err(rustix::io::Errno::PROTO.into());
                 }
 
                 Err(DispatchError::BadMessage { interface, sender_id, opcode }) => {
@@ -199,7 +198,7 @@ impl<D> WaylandSource<D> {
                         opcode,
                     );
 
-                    break Err(Errno::EPROTO.into());
+                    break Err(rustix::io::Errno::PROTO.into());
                 }
             }
         }
@@ -211,7 +210,7 @@ impl<D> WaylandSource<D> {
 
             WaylandError::Protocol(err) => {
                 log_error!("Protocol error received on display: {}", err);
-                Errno::EPROTO.into()
+                rustix::io::Errno::PROTO.into()
             }
         })
     }
